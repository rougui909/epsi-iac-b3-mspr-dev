import dns.update
import dns.resolver
import dns.query
import socket

def get_hostname():
    hostname = socket.gethostname()
    return hostname
def add_dns_record(hostname, ip_address):
    # Créer un objet Resolver pour interroger le serveur DNS primaire
    resolver = dns.resolver.Resolver()
    resolver.nameservers = ['192.168.31.140']

    # Créer un objet Update pour ajouter un enregistrement DNS
    update = dns.update.Update('cma4.box')
    update.add(hostname, 300, 'A', ip_address)

    # Envoyer la mise à jour au serveur DNS
    response = dns.query.tcp(update, '192.168.31.140')

    # Vérifier la réponse du serveur DNS
    if response.rcode() != 0:
        raise Exception('Erreur lors de la mise à jour des enregistrements DNS')

    print('Enregistrement DNS ajouté avec succès')


add_dns_record(get_hostname(), '192.168.31.142') 
