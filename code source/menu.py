from tkinter import *
from scan_réseau import *
import threading
from ping import *
from tableau_de_bord import *

# Fonction pour afficher les informations réseau
def afficher_info_reseau():
    ip_address = get_ip_address()
    hostname = get_hostname()
    public_ip = get_public_ip()
    name_ddns = get_name_ddns()
    internet_status = get_internet_status()
    network_devices = get_network_devices()


    resultats_text.configure(state="normal")
    resultats_text.delete(1.0, END)
    resultats_text.insert(END, "Adresse IP locale :\n {}\n\n".format(ip_address))
    resultats_text.insert(END, "Nom de la machine :\n {}\n\n".format(hostname))
    resultats_text.insert(END, "Adresse IP publique :\n {}\n\n".format(public_ip))
    resultats_text.insert(END, "Adresse IP publique :\n {}\n\n".format(name_ddns))
    resultats_text.insert(END, "État de la connexion Internet :\n {}\n\n".format(internet_status))
    resultats_text.insert(END, "Liste des machines détectées sur le réseau :\n{}".format(network_devices))
    resultats_text.configure(state="disabled")

def lancer_scan():
    scan_reseau_et_ports("192.168.31.0/24", resultats_text)

def test_ping():
    test_internet(resultats_text)

def lancer_test_internet():
    thread = threading.Thread(target=test_ping)
    thread.start()

fenetre = Tk()
fenetre.title("Scan de réseau et de ports")


resultats_text = Text(fenetre)
resultats_text.configure(state="disabled")
resultats_text.pack()



bar_de_menu = Menu(fenetre)

file = Menu(bar_de_menu, tearoff=0)
file.add_command(label="Tableau de bord", command=afficher_info_reseau)
bar_de_menu.add_cascade(label="Affichage", menu=file )

fonction = Menu(bar_de_menu, tearoff=0)

fonction.add_command(label="Lancer un Scan", command=lancer_scan)
fonction.add_command(label="Test de débit")
fonction.add_command(label="Test de Latence", command= lancer_test_internet)
fonction.add_command(label="Recueil Dns")

bar_de_menu.add_cascade(label="Fonctionnalités", menu=fonction)



fenetre.config(menu=bar_de_menu)

fenetre.mainloop()
