import os
import time

from tkinter import *

def test_internet(resultats_text):
    while True:
        # Exécuter la commande ping
        response = os.system("ping -c 1 google.com")

        # Vérifier si la réponse est 0 (c'est-à-dire pas d'erreur)
        if response == 0:
            resultats_text.configure(state="normal")
            resultats_text.delete("1.0", END)
            resultats_text.insert("1.0","Internet is up")
            resultats_text.configure(state="disabled")
        else:
            resultats_text.configure(state="normal")
            resultats_text.delete("1.0", END)
            resultats_text.insert("1.0","Internet is Down")
            resultats_text.configure(state="disabled")
        time.sleep(30)



